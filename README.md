Docker container running Ubuntu 14.04 LTS with
analysis tools for Colin Rundel.

To build the r-c-blas container:

     ./build

To run the container

     ./run

The container is probably being run in a cloud somewhere and we
assume that some persistent storage is mounted at

     /srv/persistent-data/

with three (self-explanatory) subdirectories:

     /srv/persistent-data/configs
     /srv/persistent-data/input
     /srv/persistent-data/output

which hold configuration info, input data, and output